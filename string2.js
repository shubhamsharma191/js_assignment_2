const string2 = (ipv4) => {
    const arr = ipv4.split(".");
    let ipv4Arr = [];
    let num;

    arr.forEach(item => {
        num = parseInt(item)
        
        // checking no char is there
        if (num.toString().length === item.length && !Number.isNaN(num)){
                ipv4Arr.push(num) 
            }

        else {
            ipv4Arr = []
        } 
    }) 

    // length of arr is 4 or not   
    if (ipv4Arr.length === 4){
        return ipv4Arr
    }
    else {
        return []
    }
}

module.exports = string2;
