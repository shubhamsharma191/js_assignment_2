const string3 = (date) => {

    const months = ["January", "February", "March", "April", "May","June", "July", "August", "September", "October", "November", "December"];
    let temp = date.split("/");
    // getting month name 
    let month = parseInt(temp[0]) ;

    
    return months[month-1];
}

module.exports = string3
