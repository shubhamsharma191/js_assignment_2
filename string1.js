const string1 = (...args)=>{

    let stringArr = [...args];
    let numberArr = [];
    
    stringArr.forEach(item => {

        let temp = item.replace("$","");
            temp  = temp.replaceAll(",",'')
        
        const num = parseFloat(temp);

        if (temp.length === num.toString().length)
         {
            numberArr.push(num)
        }
        else
         {
            return 0;
        }
        
    }) 

    return numberArr;
}
module.exports = string1
