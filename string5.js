const string5 = (stringArr) => {

    let ans = "";

    for( let i = 0; i < stringArr.length; i++){

        ans = ans + " " + stringArr[i];

        if(i === stringArr.length - 1){
            // removing whitespace in the start
            ans = ans.trim()
            ans = ans + ".";
        }
    }
    return ans;
 }

 module.exports = string5;
 